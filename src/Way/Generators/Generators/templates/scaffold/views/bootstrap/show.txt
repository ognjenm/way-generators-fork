@extends('layout.default')

@section('content')

<h1>{{ Lang::get('{{models}}.titleshow') }}</h1>

<p>{{ link_to_route('{{models}}.index', Lang::get('{{models}}.listall'),'',array('class'=>'btn btn-info')) }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			{{headings}}
		</tr>
	</thead>

	<tbody>
		<tr>
			{{fields}}
		</tr>
	</tbody>
</table>

@stop

@section('scripts')
@stop
